tool
extends HSlider


func _ready():
	$Value.text = str(value)
	
func _on_value_changed(value):
	$Value.text = str(value)


func _on_Value_text_entered(new_text):
	value = round(float(new_text))
