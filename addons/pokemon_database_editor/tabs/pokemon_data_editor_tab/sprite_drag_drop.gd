tool
extends TextureRect

func can_drop_data(position, data): 
	if data["files"][0].ends_with(".png"):
		return true

func drop_data(position, data):
	texture = load(data["files"][0])
