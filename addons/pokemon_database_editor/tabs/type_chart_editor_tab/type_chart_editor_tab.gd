tool
extends ColorRect

onready var util = preload("res://utilities/general_utilities.gd")
onready var typing_directory = Directory.new()

# Contains both the sidebar instance and data scene
var row
var editor = EditorPlugin.new()
var type_charts = []
var chart_directory = Directory.new()

func _ready():
	
	type_charts.clear()
	var popup = $ScrollContainer/ColorRect/CurrentlyEditing/Value.get_popup()
	popup.clear()
	
	if chart_directory.open("res://Backend/TypeCharts/") == OK:
		chart_directory.list_dir_begin()
		
		var file_name = chart_directory.get_next()
		while file_name != "":
			if not chart_directory.current_is_dir():
				var chart = load(chart_directory.get_current_dir() + "/" + file_name)
				type_charts.append(chart)
				popup.add_item(file_name)
				
			file_name = chart_directory.get_next()
	
	_load_Icons(type_charts[0])
	_load_All(type_charts[0])
	$ScrollContainer/ColorRect/CurrentlyEditing/Value.select(0)

func _load_All(chart) -> void:
	
	util.delete_all_children($ScrollContainer/ColorRect/VBoxContainer)
	
	for type in chart.TYPES.values():
		
		var row = HBoxContainer.new()
		
		for i in range(len(chart.TYPES)):
			var cell = load("res://addons/pokemon_database_editor/tabs/type_chart_editor_tab/type_chart_cell.tscn").instance()
			row.add_child(cell)
		
		
		for i in range(len(chart.effectiveness[int(type)])):
				if chart.effectiveness[int(type)][i] == 0.5 or chart.effectiveness[int(type)][i] == 2 or chart.effectiveness[int(type)][i] == 0:
					row.get_child(i).text = str(chart.effectiveness[int(type)][i])
					pass
				
		
		$ScrollContainer/ColorRect/VBoxContainer.add_child(row)

func _load_Icons(chart):
	var icons_horizontal = $ScrollContainer/ColorRect/HBoxContainer
	var icons_vertical = $ScrollContainer/ColorRect/VBoxContainer2 
	
	util.delete_all_children(icons_horizontal)
	util.delete_all_children(icons_vertical)
	
	for icon_path in chart.ICONS.values():
		var icon = TextureRect.new()
		icon.texture = icon_path
		icons_horizontal.add_child(icon)
		icon = TextureRect.new()
		icon.texture = icon_path
		icons_vertical.add_child(icon)

func _on_Save_pressed():

	var chart = type_charts[$ScrollContainer/ColorRect/CurrentlyEditing/Value.selected]
	var effectiveness = []
	for a in range(len(chart.TYPES)):
		effectiveness.append([])
		for b in range(len(chart.TYPES)):
			effectiveness[a].append(1)
	
	for row in $ScrollContainer/ColorRect/VBoxContainer.get_children():
		var cells = row.get_children()

		for cell in cells:
			if cell.text == "0" or cell.text == "0.5" or cell.text == "2":
				#print_debug("in " + chart.resource_name + String(row.get_index()) + " " + cell.text + " " + String(cell.get_index()))
				effectiveness[int(row.get_index())][int(cell.get_index())] = float(cell.text)
				#print_debug("changed relation between types " + String(row.get_index()) + " and " + String(cell.get_index()) + " in chart " + chart.resource_path + " to " + cell.text)
	chart.effectiveness = effectiveness

func _on_Value_item_selected(index):
	_load_Icons(type_charts[index])
	_load_All(type_charts[index])
