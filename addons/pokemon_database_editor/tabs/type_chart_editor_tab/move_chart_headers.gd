tool
extends ColorRect

onready var horizontal_header = $HorizontalHeader
onready var vertical_header = $VerticalHeader

func _process(delta):
	var n = $VBoxContainer.get_child_count()
	if not n == 0:
		horizontal_header.rect_position.x = 42 + n*27
		vertical_header.rect_position.y = 202 + n*27
	else:
		horizontal_header.visible = false
		vertical_header.visible = false
