extends Resource
class_name Types

enum TYPES {
	NORMAL,
	FIRE,
	WATER,
	ELECTRIC,
	GRASS,
	ICE,
	FIGHTING,
	POISON,
	GROUND,
	FLYING,
	PSYCHIC,
	BUG,
	ROCK,
	GHOST,
	DRAGON,
	DARK,
	STEEL,
	FAIRY
}

const ICONS = {
	TYPES.NORMAL : 		preload("res://Graphics/UI/TypeIcons/normal_icon.png"),
	TYPES.FIRE : 		preload("res://Graphics/UI/TypeIcons/fire_icon.png"),
	TYPES.WATER : 		preload("res://Graphics/UI/TypeIcons/water_icon.png"),
	TYPES.ELECTRIC : 	preload("res://Graphics/UI/TypeIcons/electric_icon.png"),
	TYPES.GRASS : 		preload("res://Graphics/UI/TypeIcons/grass_icon.png"),
	TYPES.ICE : 		preload("res://Graphics/UI/TypeIcons/ice_icon.png"),
	TYPES.FIGHTING : 	preload("res://Graphics/UI/TypeIcons/fighting_icon.png"),
	TYPES.POISON : 		preload("res://Graphics/UI/TypeIcons/poison_icon.png"),
	TYPES.GROUND : 		preload("res://Graphics/UI/TypeIcons/ground_icon.png"),
	TYPES.FLYING : 		preload("res://Graphics/UI/TypeIcons/flying_icon.png"),
	TYPES.PSYCHIC : 	preload("res://Graphics/UI/TypeIcons/psychic_icon.png"),
	TYPES.BUG : 		preload("res://Graphics/UI/TypeIcons/bug_icon.png"),
	TYPES.ROCK : 		preload("res://Graphics/UI/TypeIcons/rock_icon.png"),
	TYPES.GHOST : 		preload("res://Graphics/UI/TypeIcons/ghost_icon.png"),
	TYPES.DRAGON : 		preload("res://Graphics/UI/TypeIcons/dragon_icon.png"),
	TYPES.DARK : 		preload("res://Graphics/UI/TypeIcons/dark_icon.png"),
	TYPES.STEEL : 		preload("res://Graphics/UI/TypeIcons/steel_icon.png"),
	TYPES.FAIRY : 		preload("res://Graphics/UI/TypeIcons/fairy_icon.png")
}

export var effectiveness = [
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 0 	, 1 	, 1 	, 0.5	, 1 	], # normal
	[1 	, 0.5	, 0.5	, 1 	, 2 	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 0.5	, 1 	, 0.5	, 1 	, 2 	, 1 	], # fire
	[1 	, 2 	, 0.5	, 1 	, 0.5	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 1 	, 1 	], # water
	[1 	, 1 	, 2 	, 0.5	, 0.5	, 1 	, 1 	, 1 	, 0 	, 2 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	], # electric
	[1 	, 0.5	, 2 	, 1 	, 0.5	, 1 	, 1 	, 0.5	, 2 	, 0.5	, 1 	, 0.5	, 2 	, 1 	, 0.5	, 1 	, 0.5	, 1 	], # grass
	[1 	, 0.5	, 0.5 	, 1 	, 2 	, 0.5	, 1 	, 1 	, 2 	, 2 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	], # ice
	[2 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 0.5	, 0.5	, 0.5	, 2 	, 0 	, 1 	, 2 	, 2 	, 0.5	], # fighting
	[1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 0.5	, 0.5	, 1 	, 1 	, 1 	, 0.5	, 0.5 	, 1 	, 1 	, 0 	, 2 	], # poison
	[1 	, 2 	, 1 	, 2 	, 0.5	, 1 	, 1 	, 2 	, 1 	, 0 	, 1 	, 0.5	, 2 	, 1 	, 1 	, 1 	, 2 	, 1 	], # ground
	[1 	, 1 	, 1 	, 0.5	, 2 	, 1 	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	, 0.5	, 1 	], # flying
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 2 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	, 1 	, 0 	, 0.5	, 1 	], # psychic
	[1 	, 0.5	, 1 	, 1 	, 2 	, 1 	, 0.5	, 0.5	, 1 	, 0.5	, 2 	, 2 	, 1 	, 0.5 	, 1 	, 2 	, 0.5	, 0.5	], # bug
	[1 	, 2 	, 1 	, 1 	, 1 	, 2 	, 0.5	, 1 	, 0.5	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	], # rock
	[0 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 1 	], # ghost
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 0 	], # dragon
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 0.5	], # dark
	[1 	, 0.5	, 0.5 	, 0.5	, 1 	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 1 	, 0.5	, 2 	], # steel
	[1 	, 0.5	, 1 	, 1 	, 1 	, 1 	, 2 	, 0.5	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 2 	, 0.5	, 1 	], # fairy
]
