static func delete_all_children(node: Node):
	for i in range (0, node.get_child_count()):
		node.get_child(i).queue_free()

static func _sort_by_index(arr: Array):
	for i in range(0, len(arr)-1):
		for j in range(0, len(arr) - i - 1):
			if arr[j].id > arr[j+1].id:
				var temp = arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = temp

static func _remove_all_files(path: String):
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if not dir.current_is_dir():
				dir.remove(path + "/" + file_name)
			file_name = dir.get_next()
	var editor = EditorPlugin.new()
	editor.get_editor_interface().get_resource_filesystem().scan()

# static func _convert_resource_data_to_database_node(res: Resource):
# 	var properties = res.get_property_list()
# 	var output_node
	
# 	match res.get_class():
# 		"PokemonDataGeneric":
# 			output_node = preload("res://addons/pokemon_database_editor/tabs/pokemon_data_editor_tab/pokemon_entry.tscn").instance()
			
			
static func _get_typing_from_index(index: int):
	var dir = Directory.new()
	if dir.open("res://Backend/Enums/Typings/") == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if not dir.current_is_dir():
				var temp = load(dir.get_current_dir() + "/" + file_name)
				if temp.id == index:
					return temp
			file_name = dir.get_next()
