extends Spatial



var ally = preload("res://utilities/Ally.gd").new()
#var ally = {
#	"name": "Glaceon",
#	"type1": Types.TYPES.ICE,
#	"type2": null,
#	"level": 50,
#	"hp": 172,
#	"currentHp": 172,
#	"attack": 123,
#	"defense": 178,
#	"specialAttack": 200,
#	"specialDefense": 161,
#	"speed": 128,
#	"moves": {
#		1: {
#			"name": "Ice Fang",
#			"power": 65,
#			"type": Types.TYPES.ICE,
#			"category": "physical"
#		},
#	}
#}

var foe = preload("res://utilities/Foe.gd").new()
#var foe = {
#	"name": "Garchomp",
#	"type1": Types.TYPES.DRAGON,
#	"type2": Types.TYPES.GROUND,
#	"level": 50,
#	"hp": 215,
#	"currentHp": 215,
#	"attack": 200,
#	"defense": 161,
#	"specialAttack": 145,
#	"specialDefense": 150,
#	"speed": 169,
#	"moves": {
#		1: {
#			"name": "Fire Fang",
#			"power": 65,
#			"type": Types.TYPES.FIRE,
#			"category": "physical"
#		},
#		2: {
#			"name": "Earth Power",
#			"power": 90,
#			"type": Types.TYPES.GROUND,
#			"category": "special"
#		},
#		3: {
#			"name": "Dragon Claw",
#			"power": 80,
#			"type": Types.TYPES.DRAGON,
#			"category": "physical"
#		},
#		4: {
#			"name": "Rock Tomb",
#			"power": 60,
#			"type": Types.TYPES.ROCK,
#			"category": "physical"
#		},
#	}
#}


enum BattleState {
	START,
	PLAYERTURN,
	ENEMYTURN,
	WIN,
	LOST
}

var currentState

var green = "50ff00"
var yellow = "e5ff00"
var red = "ff3000"

var overlay

# Called when the node enters the scene tree for the first time.
func _ready():
	overlay = preload("res://Utilities/debug_overlay.tscn").instance()
	overlay.add_stat("State", self, "currentState", false)
	
	add_child(overlay)
	
	currentState = BattleState.START
	SetupBattle()


func SetupBattle():
	$CanvasLayer/Control/dialogueBox/RichTextLabel.bbcode_text = "A wild " + foe.name + " appeared..."
	
	$"CanvasLayer/Control/Ally_HPBar/HP-Numbers".bbcode_text = str(ally.currentHP) + "/" + str(ally.HP)
	SetHUD()
	
	yield(get_tree().create_timer(2), "timeout")
	
	currentState = BattleState.PLAYERTURN
	PlayerTurn()
	pass


func PlayerTurn():
	$CanvasLayer/Control/dialogueBox/RichTextLabel.bbcode_text = "What will " + ally.name + " do?"
	pass


func PlayerAttack():
	get_tree().get_root().set_disable_input(true)
	
	$CanvasLayer/Control/dialogueBox/RichTextLabel.bbcode_text = "Your " + ally.name + " used " + ally.moves[0].name
	
	yield(get_tree().create_timer(1), "timeout")
	
	# damage enemy
	for i in range(damageCalculation(ally, foe, ally.moves[0])):
		foe.currentHP -= 1
		SetHP("foe")
		yield(get_tree().create_timer(0.00005), "timeout")
		
	
	
	yield(get_tree().create_timer(1), "timeout")
	
	# check if enemy is dead
	# change state based on what happened
	if foe.currentHP <= 0:
		currentState = BattleState.WIN
		PokemonDead("foe")
		EndBattle()
	else:
		currentState = BattleState.ENEMYTURN
		EnemyTurn()
	
	
	pass


func PokemonDead(battler):
	if battler == "foe":
		for i in range(70):
			$battlefoor_foe/AnimatedSprite3D.translation -= Vector3(0, 0.1, 0)
			yield(get_tree().create_timer(0.000001), "timeout")
	elif battler == "ally":
		for i in range(70):
			$battlefoor_ally/AnimatedSprite3D.translation -= Vector3(0, 0.1, 0)
			yield(get_tree().create_timer(0.000001), "timeout")
	pass


func EnemyTurn():
	randomize()
	var selectedMove = foe.moves[randi() % 4]
	$CanvasLayer/Control/dialogueBox/RichTextLabel.bbcode_text = "Wild " + foe.name + " used " + selectedMove.name
	
	yield(get_tree().create_timer(1), "timeout")
	
	# damage enemy
	var damage = damageCalculation(foe, ally, selectedMove)
	for i in range(damage):
		ally.currentHP -= 1
		SetHP("ally")
		yield(get_tree().create_timer(0.00005), "timeout")
	
	yield(get_tree().create_timer(1), "timeout")
	
	get_tree().get_root().set_disable_input(false)
	
	# check if enemy is dead
	# change state based on what happened
	if ally.currentHP <= 0:
		currentState = BattleState.LOST
		PokemonDead("ally")
		EndBattle()
	else:
		currentState = BattleState.PLAYERTURN
		PlayerTurn()
	pass


func EndBattle():
	if currentState == BattleState.WIN:
		$CanvasLayer/Control/dialogueBox/RichTextLabel.bbcode_text = "Wild " + foe.name + " fainted."
	elif currentState == BattleState.LOST:
		$CanvasLayer/Control/dialogueBox/RichTextLabel.bbcode_text = "Your " + ally.name + " fainted."
	pass


func onAttackSelected():
	if currentState != BattleState.PLAYERTURN:
		return
	
	PlayerAttack()
	
	pass




func SetHUD():
	$CanvasLayer/Control/Ally_HPBar/Name.bbcode_text = ally.name
	$CanvasLayer/Control/Ally_HPBar/Level.bbcode_text = " " + str(ally.level)
	$CanvasLayer/Control/Ally_HPBar/hp.rect_size.x = 73 * (float(ally.currentHP) / float(ally.HP))
	
	$CanvasLayer/Control/Foe_HPBar/Name.bbcode_text = foe.name
	$CanvasLayer/Control/Foe_HPBar/Level.bbcode_text = " " + str(foe.level)
	$CanvasLayer/Control/Foe_HPBar/hp.rect_size.x = 73 * (float(foe.currentHP) / float(foe.HP))


func SetHP(battler):
	if battler == "foe":
		$CanvasLayer/Control/Foe_HPBar/hp.rect_size.x = 73 * (float(foe.currentHP) / float(foe.HP))
		if (float(foe.currentHP) / float(foe.HP)) < 0.2:
			$CanvasLayer/Control/Foe_HPBar/hp.modulate = red
		elif (float(foe.currentHP) / float(foe.HP)) < 0.5:
			$CanvasLayer/Control/Foe_HPBar/hp.modulate = yellow
		else:
			$CanvasLayer/Control/Foe_HPBar/hp.modulate = green
		pass
	elif battler == "ally":
		$CanvasLayer/Control/Ally_HPBar/hp.rect_size.x = 73 * (float(ally.currentHP) / float(ally.HP))
		$"CanvasLayer/Control/Ally_HPBar/HP-Numbers".bbcode_text = str(ally.currentHP) + "/" + str(ally.HP)
		if (float(ally.currentHP) / float(ally.HP)) < 0.2:
			$CanvasLayer/Control/Ally_HPBar/hp.modulate = red
		elif (float(ally.currentHP) / float(ally.HP)) < 0.5:
			$CanvasLayer/Control/Ally_HPBar/hp.modulate = yellow
		else:
			$CanvasLayer/Control/Ally_HPBar/hp.modulate = green


func checkEffectiveness(defender, selectedMove):
	var effectiveness = 1
	# check first type effectiveness
	if defender.types[0] != null:
		effectiveness = Types.effectiveness[selectedMove.type][defender.types[0]]
	# check second type effectiveness
	if defender.types[1] != null:
		effectiveness *= Types.effectiveness[selectedMove.type][defender.types[1]]
	return effectiveness


func damageCalculation(attacker, defender, move):
	var level = attacker.level # Level is the level of the attacking Pokémon (or twice the level for a critical hit in Generation I).
	var power = move.power # Power is the effective power of the used move.
	var a = attacker.Attack if move.category == "physical" else attacker.SpecialAttack # A is the effective Attack stat of the attacking Pokémon if the used move is a physical move, or the effective Special Attack stat of the attacking Pokémon if the used move is a special move (ignoring allGen. II/negativeGen. III+ stat stages for a critical hit).
	var d = defender.Defense if move.category == "physical" else defender.SpecialDefense # D is the effective Defense stat of the target if the used move is a physical move or a special move that uses the target's Defense stat, or the effective Special Defense of the target if the used move is an other special move (ignoring allGen. II/positiveGen. III+ stat stages for a critical hit).
	
	var targets = 1 # Targets is 0.75 if the move has more than one target, and 1 otherwise. (In Generation III, it is 0.5 for moves that target all adjacent foes with more than one target, and 1 otherwise.)
	var weather = 1 # Weather is 1.5 if a Water-type move is being used during rain or a Fire-type move during harsh sunlight, and 0.5 if a Water-type move is used during harsh sunlight or a Fire-type move during rain, and 1 otherwise.
	var badge = 1 # Badge is applied in Generation II only. It is 1.25 if the attacking Pokémon is controlled by the player and if the player has obtained the Badge corresponding to the used move's type, and 1 otherwise.
	var critical = 1 # Critical is applied starting in Generation II. It is 2 for a critical hit in Generations II-V, 1.5 for a critical hit from Generation VI onward, and 1 otherwise.
	var random = rand_range(0.85, 1.00) # random is a random factor between 0.85 and 1.00 (inclusive):
					# From Generation III onward, it is a random integer percentage between 0.85 and 1.00 (inclusive)
					# In Generations I and II, it is realized as a multiplication by a random uniformly distributed integer between 217 and 255 (inclusive), followed by an integer division by 255
	var stab = 1.5 if move.type == attacker.types[0] or move.type == attacker.types[1] else 1 # STAB is the same-type attack bonus. This is equal to 1.5 if the move's type matches any of the user's types, 2 if the user of the move additionally has Adaptability, and 1 if otherwise
	var type = checkEffectiveness(defender, move) # Type is the type effectiveness. This can be 0 (ineffective); 0.25, 0.5 (not very effective); 1 (normally effective); 2, or 4 (super effective), depending on both the move's and target's types.
	var burn = 1 # Burn is 0.5 (from Generation III onward) if the attacker is burned, its Ability is not Guts, and the used move is a physical move (other than Facade from Generation VI onward), and 1 otherwise.
	var other = 1 # other is 1 in most cases, and a different multiplier when specific interactions of moves, Abilities, or items take effect:
	
	var modifier = targets * weather * badge * critical * random * stab * type * burn * other
	
	var damage = floor(((((((2*level)/5) + 2) * power * a/d) / 50) + 2) * modifier)
	print("damage taken: " + str(damage))
	print("damage percentage: " + str(damage/defender.HP))
	print(Types.TYPES.keys())
	return damage
	
	pass





# Called every frame. 'delta' is the elapsed time since the previous frame.
func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and currentState == BattleState.PLAYERTURN:
		onAttackSelected()
	pass
