extends Resource
class_name ItemData

enum Pocket {
	ITEMS,
	POKE_BALLS,
	MACHINES,
	BERRIES,
	KEY_ITEMS
}

#General

export(int) var id: int
export(String) var name: String
export(int) var cost: int
export(String) var description: String
export(int) var fling_power: int
export(Pocket) var bag_pocket

#Sprites

export(Texture) var icon
